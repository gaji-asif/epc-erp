<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErpTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('erp_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('transaction_date')->nullable();
            $table->string('description')->nullable();
            $table->string('voucher_no')->nullable();
            $table->integer('period_id')->nullable();
            $table->string('type')->nullable()->comment('R for Receive, P for Payment, JV for journal vocher');
            $table->integer('total_transaction')->nullable();
            $table->integer('project_id')->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('erp_transactions');
    }
}
