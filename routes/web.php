<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['revalidate','auth']], function(){
	Route::get('/', 'HomeController@index');
	Route::get('/home', 'HomeController@index');
	Route::get('/dashboard', 'HomeController@index');


    // project
    Route::resource('project', 'ErpProjectController');
    Route::get('deleteProjectView/{id}', 'ErpProjectController@deleteProjectView');
    Route::get('deleteProject/{id}', 'ErpProjectController@deleteProject');

//Chart of Accounts Category
    Route::resource('account-category', 'ErpAccountsCategoryController');
    Route::get('deleteAccountCategoryView/{id}', 'ErpAccountsCategoryController@deleteAccountCategoryView');
    Route::get('deleteAccountCategory/{id}', 'ErpAccountsCategoryController@deleteAccountCategory');

//chart of Accounts Class
    Route::resource('account-class', 'ErpAccountsClassController');
    Route::get('deleteAccountClassView/{id}', 'ErpAccountsClassController@deleteAccountClassView');
    Route::get('deleteAccountClass/{id}', 'ErpAccountsClassController@deleteAccountClass');

// Chart of Accounts
    Route::get('account-list', 'ErpChartOfAccountsController@accountList');
    Route::get('add-new-coa', 'ErpChartOfAccountsController@addNewCoa');
    Route::post('save-coa-data', 'ErpChartOfAccountsController@saveCoaData');
    Route::get('edit-coa/{id}', 'ErpChartOfAccountsController@editCoa');
    Route::post('editCoa', 'ErpChartOfAccountsController@editCoa');


//Client route
    Route::resource('client', 'ErpClientController');
    Route::get('deleteClientView/{id}', 'ErpClientController@deleteClientView');
    Route::get('deleteClient/{id}', 'ErpClientController@deleteClient');

// Role route
    Route::resource('role', 'ErpRoleController');
    Route::get('deleteRoleView/{id}', 'ErpRoleController@deleteRoleView');
    Route::get('deleteRole/{id}', 'ErpRoleController@deleteRole');
    Route::get('assign-permission/{role_id}', 'ErpRoleController@assignPermission');
    Route::post('role_permission_store', 'ErpRoleController@rolePermissionStore');

// User route
    Route::resource('user', 'ErpUserController');
    Route::get('deleteUserView/{id}', 'ErpUserController@deleteUserView');
    Route::get('deleteUser/{id}', 'ErpUserController@deleteUser');

//chart of Accounts Period
    Route::resource('period', 'ErpPeriodController');
    Route::get('deletePeriodView/{id}', 'ErpPeriodController@deletePeriodView');
    Route::get('deletePeriod/{id}', 'ErpPeriodController@deletePeriod');

// Base group routes
    Route::resource('base_group', 'ErpBaseGroupController');
    Route::get('deleteBaseGroupView/{id}', 'ErpBaseGroupController@deleteBaseGroupView');
    Route::get('deleteBaseGroup/{id}', 'ErpBaseGroupController@deleteBaseGroup');

// Base setup routes
    Route::resource('base_setup', 'ErpBaseSetupController');
    Route::get('deleteBaseSetupView/{id}', 'ErpBaseSetupController@deleteBaseSetupView');
    Route::get('deleteBaseSetup/{id}', 'ErpBaseSetupController@deleteBaseSetup');

// Designation routes
    Route::resource('designation', 'ErpDesignationController');
    Route::get('deleteDesignationView/{id}', 'ErpDesignationController@deleteDesignationView');
    Route::get('deleteDesignation/{id}', 'ErpDesignationController@deleteDesignation');

// Department routes
    Route::resource('department', 'ErpDepartmentController');
    Route::get('deleteDepartmentView/{id}', 'ErpDepartmentController@deleteDepartmentView');
    Route::get('deleteDepartment/{id}', 'ErpDepartmentController@deleteDepartment');

// Employee routes
    Route::resource('employee', 'ErpEmployeeController');
    Route::get('deleteEmployeeView/{id}', 'ErpEmployeeController@deleteEmployeeView');
    Route::get('deleteEmployee/{id}', 'ErpEmployeeController@deleteEmployee');
    Route::get('checkDuplicateEmail','ErpEmployeeController@checkDuplicateEmail');

// Module links routes
    Route::resource('module_link', 'ErpModuleLinksController');
    Route::get('deleteModuleLinkView/{id}', 'ErpModuleLinksController@deleteModuleLinkView');
    Route::get('deleteModuleLink/{id}', 'ErpModuleLinksController@deleteModuleLink');

    Route::get('coa_view', 'ErpChartOfAccountsController@coa_view');
    Route::get('addAccountModal/{parentId?}','ErpChartOfAccountsController@showAddAccountModal');

// Transaction routes

    Route::get('add_transactions/{transactionType?}','TransactionsController@index')->name('addTransactionView');
    Route::post('get_transactions_form','TransactionsController@index');
    Route::post('add_transactions',[
        'as' => 'addTransactions',
        'uses'=>'TransactionsController@addTransactions'
    ]);

    //Route for trail balance
    Route::get('trial_balance','ReportController@generateTrialBalance');

    // Leave type
    Route::resource('leave_type', 'ErpLeaveTypeController');
    Route::get('deleteLeaveTypeView/{id}', 'ErpLeaveTypeController@deleteLeaveTypeView');
    Route::get('deleteLeaveType/{id}', 'ErpLeaveTypeController@deleteLeaveType');

    // Leave request
    Route::resource('leave_request', 'ErpLeaveRequestController');
    Route::get('deleteLeaveRequestView/{id}', 'ErpLeaveRequestController@deleteLeaveRequestView');
    Route::get('deleteLeaveRequest/{id}', 'ErpLeaveRequestController@deleteLeaveRequest');

});


