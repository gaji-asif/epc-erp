@extends('backEnd.master')

@section('styles')
    <link rel="stylesheet" href="{{asset('public/assets/css/addTransaction.css')}}">
@endsection

@section('mainContent')
    @if($errors->any())

        <div class="alert alert-danger" role="alert">

            @foreach($errors->getMessages() as $this_error)

                {{$this_error[0]}}

            @endforeach

        </div>

    @endif

    <div class="card">
        <div class="card-header">
            <h4 class="add-transaction-header">Journal Voucher Entry</h4>

            <form method="post" action="{{URL::to('/get_transactions_form')}}">
                @csrf
                <div class="form-group transaction-type">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="radio" name="transactionType" value="journal" id="option1" onchange="this.form.submit()" autocomplete="off" @if($transactionForm == 'journal') checked @endif>Journal voucher
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="transactionType" value="expense" id="option2" onchange="this.form.submit()" autocomplete="off" @if($transactionForm == 'expense') checked @endif> Expense
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="transactionType" value="income" id="option3" onchange="this.form.submit()" autocomplete="off" @if($transactionForm == 'income') checked @endif> Income
                        </label>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-block">
            <input type="hidden" name="url" id="url" value="{{URL::to('/')}}">
            <form id="voucher-form" method="POST" action="{{route('addTransactions')}}">
                @csrf

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="project" class="col-form-label">Select project</label>
                            <select class="js-example-basic-single col-sm-12" required name="project" id="project">
                                <option disabled selected value="">Select a project</option>
                                @foreach($projects as $project)
                                    <option value="{{$project->id}}">{{$project->project_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="transaction_date">Transaction date:</label>
                        <input type="" class="form-control datepicker" autocomplete="off" required id="transaction_date" name="transaction_date"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="transaction_type">Voucher No:</label>
                        <input type="text" readonly class="form-control" required value="{{$voucherNo}}" name="voucher_no"/>
                    </div>
                </div>

                <div class="row">

                    <div id="transaction-table-container" class="col-md-12">

                        @if($transactionForm == 'journal')
                            @include('backEnd/transaction/partials/journalVoucher')
                        @endif

                        @if($transactionForm == 'income')
                            @include('backEnd/transaction/partials/income')
                        @endif

                        @if($transactionForm == 'expense')
                            @include('backEnd/transaction/partials/expense')
                        @endif


                    </div>

                </div>

            </form>
        </div>
    </div>

@endsection

@section('javascript')

    <script src="{{asset('public/js/addTransactions.js')}}"></script>

@endsection