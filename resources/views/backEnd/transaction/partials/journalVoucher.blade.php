<table class="table table-sm voucher-table">
    <thead>
    <tr class="table-info">
        <th scope="col">Head</th>
        <th scope="col">Debit</th>
        <th scope="col">Credit</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <tr class="table-success">
        <td>
            <div class="form-group coa_category">
                <select class="form-control head-select js-example-basic-single" required name="coa_parent0">
                    @if(isset($categories))
                        @foreach($categories as $category)
                            <optgroup label="{{$category->category_name}}">
                                @foreach($category->subCategoryAccounts as $subCategory)
                                    <option value="{{$subCategory->id}}">{{$subCategory->coa_name}}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    @endif
                </select>
            </div>
        </td>
        <td>
            <input type="number" value="0" required class="form-control debit-input input-debit0" name="debit0"/>
        </td>
        <td>
            <input type="number" value="0" required class="form-control credit-input input-credit0" name="credit0"/>
        </td>
        <td></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td>
            <div class="row">
                <input type="button" class="btn btn-info" id="addrow" value="Add new" />
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="transaction_type">Total Debit:</label>
                <input type="text" readonly class="form-control debit-total inline-input" value="0" name="total_debit"/>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="transaction_type">Total Credit:</label>
                <input type="text" readonly class="form-control credit-total inline-input" value="0" name="total_credit"/>
            </div>
        </td>
        <td>
            <input type="hidden" name="transactionType" id="transactionType" value="journal">
            <input type="hidden" name="totalRow" id="totalRow" value="0">
            <input type="submit" class="btn btn-info" value="Save" />
        </td>
    </tr>
    </tfoot>
</table>