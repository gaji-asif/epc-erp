
@extends('backEnd.master')
@section('mainContent')

<div class="tab-pane" id="contacts" role="tabpanel">
	<div class="row">
    	<div class="col-xl-12">
            <div class="tab-header card">
                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Project Details</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#payments" role="tab">Payments</a>
                        <div class="slide"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#provided_documents" role="tab">Provided Documents</a>
                        <div class="slide"></div>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="personal" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            @if( isset($editData->project_name) )
                                <h5 class="card-header-text">{{ $editData->project_name }}</h5>
                            @else
                                <h5 class="card-header-text">No Project Name</h5>
                            @endif
                        </div>
                        <div class="card-block">
                            <div class="view-info">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="general-info">
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <div class="table-responsive">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                
                                                                <tr>
                                                                    <th scope="row">Project Name</th>
                                                                    @if( isset($editData->project_name) )
    																	<td>{{ $editData->project_name }}</td>
    																@else
    																	<td>No input given</td>
    																@endif
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Client name</th>

                                                                    @if( isset($clients) && isset($editData->client_id))
                                                                        @foreach($clients as $client)
                                                                            @if($client->id == $editData->client_id)
                                                                                <td>{{ $client->client_name }}</td>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif

                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Project Start Date</th>
                                                                    @if( isset($editData->project_start_date) )
    																	<td>{{ date('d-M-Y', strtotime($editData->project_start_date)) }}</td>
    																@else
    																	<td>No input given</td>
    																@endif                                                            
    															</tr>

                                                                <tr>
                                                                    <th scope="row">Project End Date</th>
                                                                    @if( isset($editData->project_end_date) )
                                                                        <td>{{ date('d-M-Y', strtotime($editData->project_end_date)) }}</td>
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif                                                            
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Project Amount</th>
                                                                    @if( isset($editData->project_amount) )
                                                                        <td>{{ $editData->project_amount }}</td>
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif                                                            
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Advances Received</th>
                                                                    @if( isset($editData->advances_received) )
                                                                        <td>{{ $editData->advances_received }}</td>
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif                                                            
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Last date of receipts</th>
                                                                    @if( isset($editData->last_date_of_receipt) )
                                                                        <td>{{ date('d-M-Y', strtotime($editData->last_date_of_receipt)) }}</td>
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif                                                            
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Completion due date</th>
                                                                    @if( isset($editData->completion_due_date) )
                                                                        <td>{{ date('d-M-Y', strtotime($editData->completion_due_date)) }}</td>
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif                                                            
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Completed on</th>
                                                                    @if( isset($editData->completed_on) )
                                                                        <td>{{ date('d-M-Y', strtotime($editData->completed_on)) }}</td>
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif                                                            
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Receipts to date</th>
                                                                    @if( isset($editData->receipts_to_date) )
                                                                        <td>{{ $editData->receipts_to_date }}</td>
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif                                                            
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Expenses to date</th>
                                                                    @if( isset($editData->expenses_to_date) )
                                                                        <td>{{ $editData->expenses_to_date }}</td>
                                                                    @else
                                                                        <td>No input given</td>
                                                                    @endif                                                            
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="payments" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Payments</h5>
                        </div>
                        <div class="card-block">
                           
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="provided_documents" role="tabpanel">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Provided Documents</h5>
                        </div>
                        <div class="card-block">
                           
                        </div>
                    </div>
                </div>

            </div>
        </div>
	</div>
</div>
@endSection