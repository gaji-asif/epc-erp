@extends('backEnd.master')
@section('mainContent')
<div class="card">
	<div class="card-header">
		<h5>Add Project</h5>
	</div>
	<div class="card-block">
		{{ Form::open(['class' => '', 'files' => true, 'action' => 'ErpProjectController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
			@csrf
			<div class="row">
				
				<div class="form-group col-md-6">
				  	<label for="project_name">Project Name:</label>
				  	<input type="text" class="form-control  {{ $errors->has('project_name') ? ' is-invalid' : '' }}" value="{{ old('project_name') }}" name="project_name" />
				  	@if ($errors->has('project_name'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('project_name') }}</strong></span>
						</span>
					@endif
				</div>

				<div class="form-group col-md-6">
				  	<label for="project_start_date">Project Start Date :</label>
				  	<input type="" class="form-control datepicker  {{ $errors->has('project_start_date') ? ' is-invalid' : '' }}" value="{{ old('project_start_date') }}" name="project_start_date"/>
				  	@if ($errors->has('project_start_date'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('project_start_date') }}</strong></span>
						</span>
					@endif
				</div>
				
			</div>
			<!-- <div class="dropdown">
					<label for="project_status">Project Status:</label>
				<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Status
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" value="1">Active</a>
				<a class="dropdown-item" value="0">Not Active</a>
				</div>
			</div> -->
			<!-- <div class="form-group">
			  <label for="project_status">Project Status:</label>
			  <input type="number" class="form-control" name="project_status"/>
			</div> -->
			<div class="row">
				
				<div class="form-group col-md-6">
				  <label for="project_end_date">Project End Date :</label>
				  <input type="" class="form-control datepicker {{ $errors->has('project_end_date') ? ' is-invalid' : '' }}" value="{{ old('project_end_date') }}" name="project_end_date"/>
				  	@if ($errors->has('project_end_date'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('project_end_date') }}</strong></span>
						</span>
					@endif
				</div>

				<div class="form-group col-md-6">
				  	<label for="project_amount">Project Amount:</label>
				  	<input type="number" class="form-control {{ $errors->has('project_amount') ? ' is-invalid' : '' }}" value="{{ old('project_amount') }}" name="project_amount"/>
				  	@if ($errors->has('project_amount'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('project_amount') }}</strong></span>
						</span>
					@endif
				</div>

			</div>
			<div class="row">
				
				<div class="form-group col-md-6">
					<label class="col-form-label">Client Name</label>
					<select class="js-example-basic-single col-sm-12 {{ $errors->has('client_id') ? ' is-invalid' : '' }}" name="client_id" id="client_id">
					<option value="">Select Client name</option>
					@if(isset($clients))
						@foreach($clients as $client)
							<option value="{{ $client->id }}"  {{ old('client_id')== $client->id ? 'selected' : ''  }}>{{$client->client_name}}</option>
						@endforeach
					@endif
					</select>
					@if ($errors->has('client_id'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('client_id') }}</strong>
					</span>
					@endif
				</div>
				
				<div class="form-group col-md-6">
				  <label for="advances_received">Advances Received:</label>
				  <input type="number" class="form-control" value="{{ old('advances_received') }}" name="advances_received"/>
				</div>

			</div>

			<div class="row">
				
				<div class="form-group col-md-6">
				  <label for="last_date_of_receipt">Last Date of Receipt:</label>
				  <input type="" class="form-control datepicker" value="{{ old('last_date_of_receipt') }}" name="last_date_of_receipt"/>
				</div>

				<div class="form-group col-md-6">
				  <label for="completion_due_date">Completion Due Date:</label>
				  <input type="" class="form-control datepicker" value="{{ old('completion_due_date') }}" name="completion_due_date"/>
				</div>

			</div>

			<div class="row">

				<div class="form-group col-md-6">
				  <label for="completed_on">Completed on:</label>
				  <input type="" class="form-control datepicker" value="{{ old('completed_on') }}" name="completed_on"/>
				</div>
				
				<div class="form-group col-md-6">
				  <label for="receipts_to_date">Receipts to Date:</label>
				  <input type="number" class="form-control" value="{{ old('receipts_to_date') }}" name="receipts_to_date"/>
				</div>

			</div>
			<!-- <div class="form-group">
			  <label for="coa_account_id">COA account ID:</label>
			  <input type="text" class="form-control" name="coa_account_id"/>
			</div>
			<div class="form-group">
			  <label for="company_id">Company ID:</label>
			  <input type="text" class="form-control" name="company_id"/>
			</div> -->
			<div class="row">

				<div class="form-group col-md-6">
				  <label for="expenses_to_date">Expenses to Date:</label>
				  <input type="number" class="form-control" value="{{ old('expenses_to_date') }}" name="expenses_to_date"/>
				</div>

			</div>

			<div class="form-group row mt-5">
				<div class="col-sm-12 text-center">
					<button type="submit" class="btn btn-primary m-b-0">Add Project</button>
				</div>
			</div>
		{{ Form::close()}}
	</div>
</div>

@endSection