@extends('backEnd.master')
@section('mainContent')

@if(session()->has('message-success'))
	<div class="alert alert-success mb-3 background-success" role="alert">
		{{ session()->get('message-success') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@elseif(session()->has('message-danger'))
	<div class="alert alert-danger">
		{{ session()->get('message-danger') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif
@if(session()->has('message-success-delete'))
	<div class="alert alert-danger mb-3 background-danger" role="alert">
		{{ session()->get('message-success-delete') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@elseif(session()->has('message-danger-delete'))
	<div class="alert alert-danger">
		{{ session()->get('message-danger-delete') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif


<div class="card">
	<div class="card-header">
		<h5>Project Lists</h5>
		<a href="{{ route('project.create') }}" style="float: right; padding: 8px;" class="btn btn-success"> Add Project </a>
	</div>
	<div class="card-block">
		<table id="basic-btn" class="table table-striped table-bordered nowrap">
			<thead>
				<tr>
					<th>Serial</th>
					<th>Project Name</th>
					<th>Project Start Date</th>
					<th>Project End Date</th>
					<th>Project Amount</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@php $i = 1 @endphp
				@foreach($projects as $project)
		        <tr>
		            <td>{{$i++}}</td>
		            <td>{{$project->project_name}}</td>
		            <td>{{ date('d-M-Y', strtotime($project->project_start_date)) }}</td>
		            <td>{{ date('d-M-Y', strtotime($project->project_end_date)) }}</td>
		            <td>{{$project->project_amount}}</td>
		            <td>
		            	<a href="{{ route('project.show',$project->id) }}" title="view"><button type="button" class="btn btn-success action-icon"><i class="fa fa-eye"></i></button></a>
						<a href="{{ route('project.edit',$project->id) }}" title="edit"><button type="button" class="btn btn-info action-icon"><i class="fa fa-edit"></i></button></a>
						<a class="modalLink" title="Delete" data-modal-size="modal-md" href="{{url('deleteProjectView', $project->id)}}">
							<button type="button" class="btn btn-danger action-icon"><i class="fa fa-trash-o"></i></button>
						</a>
		            </td>
		            
		        </tr>
		        @endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>Project ID</th>
					<th>Project Name</th>
					<th>Project Start Date</th>
					<th>Project End Date</th>
					<th>Project Amount</th>
					<th>Actions</th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
@endSection