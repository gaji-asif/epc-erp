@extends('backEnd.master')
@section('mainContent')
<div class="card">
	<div class="card-header">
		<h5>Add Leave Request</h5>
	</div>
	<div class="card-block">
		{{ Form::open(['class' => '', 'files' => true, 'url' => 'leave_request', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}

			<div class="row">
				
				<div class="form-group col-md-6">
					<label class="col-form-label">Leave Type</label>
					<select class="js-example-basic-single col-sm-12 {{ $errors->has('type_id') ? ' is-invalid' : '' }}" name="type_id" id="type_id">
					<option value="">Select Leave Type</option>
					@if(isset($leave_types))
						@foreach($leave_types as $leave_type)
							<option value="{{ $leave_type->id }}"  {{ old('leave_type')== $leave_type->id ? 'selected' : ''  }}>{{$leave_type->type}}</option>
						@endforeach
					@endif
					</select>
					@if ($errors->has('type_id'))
					<span class="invalid-feedback invalid-select" role="alert">
						<strong>{{ $errors->first('type_id') }}</strong>
					</span>
					@endif
				</div>
				
				<div class="form-group col-md-6">
				  	<label for="apply_date">Apply Date :</label>
				  	<input type="" class="form-control datepicker  {{ $errors->has('apply_date') ? ' is-invalid' : '' }}" value="{{ old('apply_date') }}" name="apply_date"/>
				  	@if ($errors->has('apply_date'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('apply_date') }}</strong></span>
						</span>
					@endif
				</div>

			</div>

			<div class="row">
				
				<div class="form-group col-md-6">
				  	<label for="leave_from">Leave From:</label>
				  	<input type="" class="form-control datepicker  {{ $errors->has('leave_from') ? ' is-invalid' : '' }}" value="{{ old('leave_from') }}" name="leave_from"/>
				  	@if ($errors->has('leave_from'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('leave_from') }}</strong></span>
						</span>
					@endif
				</div>

				<div class="form-group col-md-6">
				  	<label for="leave_to">Leave To:</label>
				  	<input type="" class="form-control datepicker  {{ $errors->has('leave_to') ? ' is-invalid' : '' }}" value="{{ old('leave_to') }}" name="leave_to"/>
				  	@if ($errors->has('leave_to'))
					    <span class="invalid-feedback" role="alert" >
							<span class="messages"><strong>{{ $errors->first('leave_to') }}</strong></span>
						</span>
					@endif
				</div>
				
			</div>

			<div class="row">

				<div class="form-group col-md-6">
				  	<label for="reason">Reason:</label>
				  	<textarea class="form-control" value="{{ old('reason') }}" name="reason"></textarea>
				  	
				  	@if ($errors->has('reason'))
					    <span class="" role="alert" >
							<span class="messages add_customized_red_error"><strong>{{ $errors->first('reason') }}</strong></span>
						</span>
					@endif
				</div>
				<div class="form-group col-md-6">
				  	<label for="reason">Upload File:</label><br>
				  	<input type="file" name="file" id="file">
				  	<br>
				  	<p>* Valid extensions are pdf, png, jpg, jpeg, doc, docx, gif, tiff, svg and file must be less then 9 MB.</p>
				</div>

			</div>

			<div class="form-group row mt-5">
				<div class="col-sm-12 text-center">
					<button type="submit" class="btn btn-primary m-b-0">Add Request</button>
				</div>
			</div>
		{{ Form::close()}}
	</div>
</div>

@endSection