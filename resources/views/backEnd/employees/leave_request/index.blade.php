@extends('backEnd.master')
@section('mainContent')

@if(session()->has('message-success'))
	<div class="alert alert-success mb-3 background-success" role="alert">
		{{ session()->get('message-success') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@elseif(session()->has('message-danger'))
	<div class="alert alert-danger">
		{{ session()->get('message-danger') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif
@if(session()->has('message-success-delete'))
	<div class="alert alert-danger mb-3 background-danger" role="alert">
		{{ session()->get('message-success-delete') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@elseif(session()->has('message-danger-delete'))
	<div class="alert alert-danger">
		{{ session()->get('message-danger-delete') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif


<div class="card">
	<div class="card-header">
		<h5>Leave Request Lists</h5>
		<a href="{{ route('leave_request.create') }}" style="float: right; padding: 8px;" class="btn btn-success"> Add Leave Request </a>
	</div>
	@if ($user_role_id == 1)
		<div class="card-block">
			<table id="basic-btn" class="table table-striped table-bordered nowrap">
				<thead>
					<tr>
						<th>Serial</th>
						<th>Leave Type Name</th>
						<th>Applied at</th>
						<th>From</th>
						<th>To</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@php $i = 1 @endphp
					@foreach($leave_requests as $leave_request)
			        <tr>
			            <td>{{$i++}}</td>
			            @if ( isset($leave_types) )
			            	@foreach ($leave_types as $leave_type) 
			            		@if ($leave_type->id == $leave_request->type_id)
			            			<td>{{$leave_type->type}}</td>
			            		@endif
			            	@endforeach
			            @else
			            	<td>No types</td>
			            @endif

			            <td>{{ date('d-M-Y', strtotime($leave_request->apply_date)) }}</td>
			            <td>{{ date('d-M-Y', strtotime($leave_request->leave_from)) }}</td>
			            <td>{{ date('d-M-Y', strtotime($leave_request->leave_to)) }}</td>
			            @if ($user_role_id == 1)
			            	<td>
								<a href="{{ route('leave_request.show',$leave_request->id) }}" title="view"><button type="button" class="btn btn-success action-icon"><i class="fa fa-eye"></i></button></a>
								<a class="modalLink" title="Delete" data-modal-size="modal-md" href="{{url('deleteLeaveRequestView', $leave_request->id)}}">
									<button type="button" class="btn btn-success action-icon">Accept</button>
								</a>
								<a class="modalLink" title="Delete" data-modal-size="modal-md" href="{{url('deleteLeaveRequestView', $leave_request->id)}}">
									<button type="button" class="btn btn-danger action-icon">Reject</button>
								</a>
				            </td>
			            @else
			            	<td>
								<a href="{{ route('leave_request.edit',$leave_request->id) }}" title="edit"><button type="button" class="btn btn-info action-icon"><i class="fa fa-edit"></i></button></a>
								<a class="modalLink" title="Delete" data-modal-size="modal-md" href="{{url('deleteLeaveRequestView', $leave_request->id)}}">
									<button type="button" class="btn btn-danger action-icon"><i class="fa fa-trash-o"></i></button>
								</a>
				            </td>
			            @endif
			            
			            
			        </tr>
			        @endforeach
				</tbody>
				<tfoot>
					<tr>
						<th>Serial</th>
						<th>Leave Type Name</th>
						<th>Applied at</th>
						<th>From</th>
						<th>To</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			</table>
		</div>
	@else
		<div class="card-block">
			<table id="basic-btn" class="table table-striped table-bordered nowrap">
				<thead>
					<tr>
						<th>Serial</th>
						<th>Leave Type Name</th>
						<th>Applied at</th>
						<th>From</th>
						<th>To</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@php $i = 1 @endphp
					@foreach($leave_requests as $leave_request)
			        <tr>
			            <td>{{$i++}}</td>
			            @if ( isset($leave_types) )
			            	@foreach ($leave_types as $leave_type) 
			            		@if ($leave_type->id == $leave_request->type_id)
			            			<td>{{$leave_type->type}}</td>
			            		@endif
			            	@endforeach
			            @else
			            	<td>No types</td>
			            @endif
			            <td>{{ date('d-M-Y', strtotime($leave_request->apply_date)) }}</td>
			            <td>{{ date('d-M-Y', strtotime($leave_request->leave_from)) }}</td>
			            <td>{{ date('d-M-Y', strtotime($leave_request->leave_to)) }}</td>
		            	<td>
							<a href="{{ route('leave_request.edit',$leave_request->id) }}" title="edit"><button type="button" class="btn btn-info action-icon"><i class="fa fa-edit"></i></button></a>
							<a class="modalLink" title="Delete" data-modal-size="modal-md" href="{{url('deleteLeaveRequestView', $leave_request->id)}}">
								<button type="button" class="btn btn-danger action-icon"><i class="fa fa-trash-o"></i></button>
							</a>
			            </td>         
			        </tr>
			        @endforeach
				</tbody>
				<tfoot>
					<tr>
						<th>Serial</th>
						<th>Leave Type Name</th>
						<th>Applied at</th>
						<th>From</th>
						<th>To</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			</table>
		</div>
	@endif
	
</div>
@endSection