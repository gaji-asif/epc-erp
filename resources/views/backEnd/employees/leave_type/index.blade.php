@extends('backEnd.master')
@section('mainContent')
<div class="row">
	<div class="col-md-4">
		@if(session()->has('message-success'))
		<div class="alert alert-success mb-3 background-success" role="alert">
			{{ session()->get('message-success') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@elseif(session()->has('message-danger'))
		<div class="alert alert-danger">
			{{ session()->get('message-danger') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		@if(session()->has('message-success-delete'))
			<div class="alert alert-danger mb-3 background-danger" role="alert">
				{{ session()->get('message-success-delete') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@elseif(session()->has('message-danger-delete'))
			<div class="alert alert-danger">
				{{ session()->get('message-danger-delete') }}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endif
		<div class="card">
			<div class="card-header">
				<h5>Add Leave Type</h5>
			</div>
			<div class="card-block">
				@if(isset($editData))
				{{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'leave_type/'.$editData->id, 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
				@else
				{{ Form::open(['class' => '', 'files' => true, 'url' => 'leave_type',
				'method' => 'POST', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
				@endif
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-form-label">Leave Type</label>
							<input type="text" class="form-control {{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="name" placeholder="Ex: Sick Leave" value="{{isset($editData)? $editData->type : old('type') }}">

							@if ($errors->has('type'))
							<span class="invalid-feedback" role="alert">
								<span class="messages"><strong>{{ $errors->first('type') }}</strong></span>
							</span>
							@endif
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-form-label">Total Days</label>
							<input type="text" class="form-control {{ $errors->has('total_days') ? ' is-invalid' : '' }}" name="total_days" id="name" placeholder="Ex: 2" value="{{isset($editData)? $editData->total_days : old('total_days') }}">

							@if ($errors->has('total_days'))
							<span class="invalid-feedback" role="alert">
								<span class="messages"><strong>{{ $errors->first('total_days') }}</strong></span>
							</span>
							@endif
						</div>
					</div>
				</div>
				<div class="form-group row mt-4">
					<div class="col-sm-12 text-center">
						<button type="submit" class="btn btn-primary m-b-0">Submit</button>
					</div>
				</div>
				{{ Form::close()}}
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="card">
			<div class="card-header">
				<h5>Leave Type Lists</h5>
			</div>
			<div class="card-block">
				<div class="dt-responsive table-responsive">
					<table id="basic-btn" class="table table-striped table-bordered nowrap">
						<thead>
							<tr>
								<th>Serial</th>
								<th>Leave Type Name</th>
								<th>Total Days</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($leave_types))
							@php $i = 1 @endphp
							@foreach($leave_types as $leave_type)
							<tr>
								<td>{{$i++}}</td>
								<td>{{$leave_type->type}}</td>
								<td>{{$leave_type->total_days}}</td>
								<td>
									<a href="{{url('leave_type/'.$leave_type->id.'/edit')}}" title="edit"><button type="button" class="btn btn-info action-icon"><i class="fa fa-edit"></i></button></a>
									<a class="modalLink" title="Delete" data-modal-size="modal-md" href="{{url('deleteLeaveTypeView', $leave_type->id)}}">
										<button type="button" class="btn btn-danger action-icon"><i class="fa fa-trash-o"></i></button>
									</a>
								</td>
							</tr>
							@endforeach
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endSection