@extends('backEnd.master')

@section('styles')
    <link rel="stylesheet" href="{{asset('public/assets/css/addTransaction.css')}}">
@endsection

@section('mainContent')
    @if($errors->any())

        <div class="alert alert-danger" role="alert">

            @foreach($errors->getMessages() as $this_error)

               {{$this_error[0]}}

            @endforeach

        </div>

    @endif

    <div class="card">
        <div class="card-header">
            <h4 class="add-transaction-header">Journal Voucher Entry</h4>

            <div class="form-group transaction-type">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary active">
                        <input type="radio" name="options" id="option1" autocomplete="off" checked>Journal voucher
                    </label>
                    <label class="btn btn-primary">
                        <input type="radio" name="options" id="option2" autocomplete="off"> Debit
                    </label>
                    <label class="btn btn-primary">
                        <input type="radio" name="options" id="option3" autocomplete="off"> Credit
                    </label>
                </div>
            </div>
        </div>
        <div class="card-block">
            <form id="voucher-form" method="POST" action="{{route('addTransactions')}}">
                @csrf

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="project" class="col-form-label">Select project</label>
                            <select class="js-example-basic-single col-sm-12" required name="project" id="project">
                                <option disabled selected value="">Select a project</option>
                                @foreach($projects as $project)
                                    <option value="{{$project->id}}">{{$project->project_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="transaction_date">Transaction date:</label>
                        <input type="" class="form-control datepicker" autocomplete="off" required id="transaction_date" name="transaction_date"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="transaction_type">Voucher No:</label>
                        <input type="text" readonly class="form-control" required value="{{$voucherNo}}" name="voucher_no"/>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">

                        <table class="table table-sm voucher-table">
                            <thead>
                            <tr class="table-info">
                                <th scope="col">Head</th>
                                <th scope="col">Debit</th>
                                <th scope="col">Credit</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="table-success">
                                <td>
                                    <div class="form-group coa_category">
                                        <select class="form-control head-select js-example-basic-single" required name="coa_parent0">
                                            @if(isset($categories))
                                                @foreach($categories as $category)
                                                    <optgroup label="{{$category->category_name}}">
                                                        @foreach($category->subCategoryAccounts as $subCategory)
                                                            <option value="{{$subCategory->id}}">{{$subCategory->coa_name}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <input type="number" value="0" required class="form-control debit-input input-debit0" name="debit0"/>
                                </td>
                                <td>
                                    <input type="number" value="0" required class="form-control credit-input input-credit0" name="credit0"/>
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td>
                                    <div class="row">
                                        <input type="button" class="btn btn-info" id="addrow" value="Add new" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="transaction_type">Total Debit:</label>
                                        <input type="text" readonly class="form-control debit-total inline-input" value="0" name="total_debit"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="transaction_type">Total Credit:</label>
                                        <input type="text" readonly class="form-control credit-total inline-input" value="0" name="total_credit"/>
                                    </div>
                                </td>
                                <td>
                                    <input type="hidden" name="totalRow" id="totalRow" value="0">
                                    <input type="submit" class="btn btn-info" value="Save" />
                                </td>
                            </tr>
                            </tfoot>
                        </table>

                    </div>

                </div>

            </form>
        </div>
    </div>

@endsection

@section('javascript')

    <script src="{{asset('public/js/addTransactions.js')}}"></script>

@endsection