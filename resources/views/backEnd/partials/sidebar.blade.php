<!-- make sure to do mind this things to add more sidebar or else there maybe some problem occur -->
<!-- 1. make sure class name and anchor tag url name are same like account-category
	Example: <li class="account-category"> and <a href="{{url('account-category')}}"> -->
<!-- 2. Don't declare any other class if you do make sure add that class in custom.js for sidebar problem -->
@php
$role_id = Auth::user()->role_id;
$module_links = [];
$modules = [];
$permissions = App\Erp_role_permissions::where('role_id', $role_id)->get();

foreach($permissions as $permission){
	$module_links[] = $permission->module_link_id;
	$modules[] = $permission->moduleLink->module_id;
}

$modules = array_unique($modules);

@endphp
<nav class="pcoded-navbar">
	<div class="sidebar_toggle"><a href=""><i class="icon-close icons"></i></a></div>
	<div class="pcoded-inner-navbar main-menu">
		<div class="">
			<div class="main-menu-header">
				<img class="img-80 img-radius" src="{{asset('public/assets/images/user_icon.png')}}" alt="User-Profile-Image">
				<div class="user-details">
					<span id="more-details">{{Auth::user()->name}}<i class="fa fa-caret-down"></i></span>
				</div>
			</div>
			<div class="main-menu-content">
				<ul>
					<li class="more-details">
						<a href=""><i class="ti-user"></i>View Profile</a>
						<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<i class="ti-layout-sidebar-left"></i>{{ __('Logout') }}
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
					</li>
				</ul>
			</div>
		</div>

		<div class="pcoded-navigation-label">Navigation</div>
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu dashboard active pcoded-trigger">
				<a href="{{url('/')}}" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
					<span class="pcoded-mtext">Dashboard</span>
					<span class="pcoded-mcaret"></span>
				</a>

			</li>

			@if(in_array(1, $modules) || Auth::user()->role_id == 1)

			<li class="pcoded-hasmenu chart_of_accounts">
				<a href="javascript:void(0)" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-layout"></i><b>P</b></span>
					<span class="pcoded-mtext">Accounts</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					@if(in_array(1,$module_links))
					<li class="account-category">
						<a href="{{url('account-category')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Account Category</span>
						</a>
					</li>
					@endif
					<li class="account-class">
						<a href="{{url('account-class')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Account Class</span>
						</a>
					</li>
					<li class="account-list">
						<a href="{{url('account-list')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Chart of Account List</span>
						</a>
					</li>
					<li class="add-new-coa">
						<a href="{{url('add-new-coa')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Add New COA</span>
						</a>
					</li>
					<li class="period">
						<a href="{{url('period')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Period</span>
						</a>
					</li>
					<li class="coa_view">
						<a href="{{url('coa_view')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">COA view</span>
						</a>
					</li>
					<li class="add_transactions">
						<a href="{{url('add_transactions')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Add Transactions</span>
						</a>
					</li>
					<li class="trail_balance">
						<a href="{{url('trial_balance')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Trial balance</span>
						</a>
					</li>
				</ul>
			</li>

			@endif

			<li class="pcoded-hasmenu projects">
				<a href="javascript:void(0)" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-agenda"></i><b>P</b></span>
					<span class="pcoded-mtext">Projects</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="project">
						<a href="{{url('project')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Project List</span>
						</a>
					</li>
				</ul>
			</li>

			<li class="pcoded-hasmenu clients">
				<a href="javascript:void(0)" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-user"></i><b>P</b></span>
					<span class="pcoded-mtext">Clients</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">

					<li class="client">
						<a href="{{url('client')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Client List</span>
						</a>
					</li>

				</ul>
			</li>

			<li class="pcoded-hasmenu employees">
				<a href="javascript:void(0)" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-agenda"></i><b>P</b></span>
					<span class="pcoded-mtext">HR and payroll</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="employee">
						<a href="{{url('employee')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Employee</span>
						</a>
					</li>
					<li class="designation">
						<a href="{{url('designation')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Designation</span>
						</a>
					</li>
					<li class="department">
						<a href="{{url('department')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Department</span>
						</a>
					</li>
					<li class="leave_type">
						<a href="{{url('leave_type')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Leave Types</span>
						</a>
					</li>
					<li class="leave_request">
						<a href="{{url('leave_request')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Leave Requests</span>
						</a>
					</li>
				</ul>
			</li>

			<li class="pcoded-hasmenu settings">
				<a href="javascript:void(0)" class="waves-effect waves-dark">
					<span class="pcoded-micon"><i class="ti-settings"></i><b>P</b></span>
					<span class="pcoded-mtext">Settings</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">

					<li class="user">
						<a href="{{url('user')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">User</span>
						</a>
					</li>
					<li class="role">
						<a href="{{url('role')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Role</span>
						</a>
					</li>
					<li class="base_group">
						<a href="{{url('base_group')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Base group</span>
						</a>
					</li>
					<li class="base_setup">
						<a href="{{url('base_setup')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Base setup</span>
						</a>
					</li>
					<li class="module_link">
						<a href="{{url('module_link')}}" class="waves-effect waves-dark">
							<span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
							<span class="pcoded-mtext">Module links</span>
						</a>
					</li>

				</ul>
			</li>

		</ul>
	</div>
</nav>