@extends('backEnd.master')

@section('mainContent')

    <table class="table table-borderless trail-balance-table">
        <thead>
        <tr>
            <th scope="col">Account</th>
            <th scope="col">Debit</th>
            <th scope="col">Credit</th>
        </tr>
        </thead>
        <tbody>
            @foreach($transactions as $transaction)

                <tr>
                    <td>{{ $transaction->account->coa_name }}</td>
                    <td>{{ $transaction->debit_amount > 0 ? $transaction->debit_amount : '' }}</td>
                    <td>{{ $transaction->credit_amount > 0 ? $transaction->credit_amount : '' }}</td>
                </tr>

            @endforeach
            
            <tr>
                <td>Total</td>
                <td>{{ $transactions->sum('debit_amount') }}</td>
                <td>{{ $transactions->sum('credit_amount')  }}</td>
            </tr>
        </tbody>
    </table>

@endsection