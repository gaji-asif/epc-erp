
@extends('backEnd.master')
@section('mainContent')

<div class="tab-pane" id="contacts" role="tabpanel">
	<div class="row">

		<div class="col-xl-3">
			<div class="card user-card">
				<div class="card-header-img">
					@if( isset($client->client_image) )
						<img class="img-fluid img-radius" style="margin-top: 20px;" src="{{ asset($client->client_image) }}" alt="card-img">
					@else
						<img class="img-fluid img-radius" src="{{ asset('/public/images/no_image.png') }}" alt="card-img">
					@endif

					@if( isset($client->client_name) )
						<h4>{{ $client->client_name }}</h4>
					@else
						<h4>No Client Name</h4>
					@endif
					
					@if( isset($client->email) )
						<h5>{{ $client->email }}</h5>
					@else
						<h5>No Email</h5>
					@endif

                    
                    <div style="text-align: center;">
                        @if( isset($client->client_contact) )
                            <h5></h5>
                            <button type="button" class="btn btn-primary waves-effect waves-light m-r-15">{{ $client->client_contact }}</button>
                        @else
                            <button type="button" class="btn btn-primary waves-effect waves-light">No Number</button>
                        @endif
                    </div>

			    </div>
		    </div>
        </div>

		 <div class="col-xl-9">
        <div class="tab-header card">
            <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Client Details</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#payments" role="tab">Payments</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#provided_documents" role="tab">Provided Documents</a>
                    <div class="slide"></div>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="personal" role="tabpanel">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Details</h5>
                    </div>
                    <div class="card-block">
                        <div class="view-info">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="general-info">
                                        <div class="row">
                                            <div class="col-lg-12 ">
                                                <div class="table-responsive">
                                                    <table class="table m-0">
                                                        <tbody>
                                                            
                                                            <tr>
                                                                <th scope="row">Client Name</th>
                                                                @if( isset($client->client_name) )
																	<td>{{ $client->client_name }}</td>
																@else
																	<td>No input given</td>
																@endif
                                                            </tr>

                                                            <tr>
                                                                <th scope="row">Client email</th>

                                                                @if( isset($client->email) )
                                                                    <td>{{ $client->email }}</td>
                                                                @else
                                                                    <td>No input given</td>
                                                                @endif

                                                            </tr>

                                                            <tr>
                                                                <th scope="row">Client Contact no</th>
                                                                @if( isset($client->client_contact) )
																	<td>{{ $client->client_contact }}</td>
																@else
																	<td>No input given</td>
																@endif                                                            
															</tr>

                                                            <tr>
                                                                <th scope="row">Client Phone no 1</th>
                                                                @if( isset($client->client_phone_1) )
                                                                    <td>{{ $client->client_phone_1 }}</td>
                                                                @else
                                                                    <td>No input given</td>
                                                                @endif                                                            
                                                            </tr>

                                                            <tr>
                                                                <th scope="row">Client Phone no 2</th>
                                                                @if( isset($client->client_phone_2) )
                                                                    <td>{{ $client->client_phone_2 }}</td>
                                                                @else
                                                                    <td>No input given</td>
                                                                @endif                                                            
                                                            </tr>

                                                            <tr>
                                                                <th scope="row">Client Fax</th>
                                                                @if( isset($client->client_fax) )
                                                                    <td>{{ $client->client_fax }}</td>
                                                                @else
                                                                    <td>No input given</td>
                                                                @endif                                                            
                                                            </tr>

                                                            <tr>
                                                                <th scope="row">Client Remarks</th>
                                                                @if( isset($client->client_remarks) )
                                                                    <td>{{ $client->client_remarks }}</td>
                                                                @else
                                                                    <td>No input given</td>
                                                                @endif                                                            
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane" id="payments" role="tabpanel">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Payments</h5>
                    </div>
                    <div class="card-block">
                       
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="provided_documents" role="tabpanel">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Provided Documents</h5>
                    </div>
                    <div class="card-block">
                       
                    </div>
                </div>
            </div>

        </div>
    </div>
	</div>
</div>
@endSection