@extends('backEnd.master')
@section('mainContent')

@if(session()->has('message-success'))
	<div class="alert alert-success mb-3 background-success" role="alert">
		{{ session()->get('message-success') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@elseif(session()->has('message-danger'))
	<div class="alert alert-danger">
		{{ session()->get('message-danger') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif

@if(session()->has('message-success-delete'))
	<div class="alert alert-danger mb-3 background-danger" role="alert">
		{{ session()->get('message-success-delete') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@elseif(session()->has('message-danger-delete'))
	<div class="alert alert-danger">
		{{ session()->get('message-danger-delete') }}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
@endif

<div class="card">
	<div class="card-header">
		<h5>Client Lists</h5>
		<a href="{{ route('client.create') }}" style="float: right; padding: 8px;" class="btn btn-success"> Add Client </a>
	</div>
	<div class="card-block">
		<table id="basic-btn" class="table table-striped table-bordered nowrap">
			<thead>
				<tr>
					<th>Serial</th>
					<th>Client Name</th>
					<th>Client Contact</th>
					<th>Email</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@php $i = 1 @endphp
				@foreach($clients as $client)
				<tr>
					<td>{{$i++}}</td>
		            <td>{{$client->client_name}}</td>
		            <td>{{$client->client_contact}}</td>
		            <td>{{$client->email}}</td>
		            <td>
		                <a href="{{ route('client.show',$client->id) }}" title="view"><button type="button" class="btn btn-success action-icon"><i class="fa fa-eye"></i></button></a>
						<a href="{{ route('client.edit',$client->id) }}" title="edit"><button type="button" class="btn btn-info action-icon"><i class="fa fa-edit"></i></button></a>
						<a class="modalLink" title="Delete" data-modal-size="modal-md" href="{{url('deleteClientView', $client->id)}}">
							<button type="button" class="btn btn-danger action-icon"><i class="fa fa-trash-o"></i></button>
						</a>
						
		            </td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>client ID</th>
					<th>Client Name</th>
					<th>Client Contact</th>
					<th>Email</th>
					<th>Actions</th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
@endSection