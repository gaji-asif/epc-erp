
@extends('backEnd.master')
@section('mainContent')

<div class="tab-pane" id="contacts" role="tabpanel">
	<div class="row">
		 <div class="col-xl-12">
            <div class="tab-header card">
                <div class="card-header">
                    <h5>Add New Chart of Account</h5>
                    <a style="float: right; padding: 8px;" class="btn btn-success modalLink" data-modal-size="modal-md" href="{{url('addAccountModal')}}">
                        Add account
                    </a>
                </div>
                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">

                    @foreach($categories as $category)

                        <li class="nav-item">
                            <a class="nav-link @if($loop->first) active @endif" data-toggle="tab" href="#{{camel_case($category->category_name)}}" role="tab">{{$category->category_name}}</a>
                            <div class="slide"></div>
                        </li>

                    @endforeach

                </ul>
            </div>
        <div class="tab-content">

            @foreach($categories as $category)

                <div class="tab-pane @if($loop->first) active @endif" id="{{camel_case($category->category_name)}}" role="tabpanel">
                    <div class="card">
                       <div class="card-block accordion-block">
                           <div id="accordion" role="tablist" aria-multiselectable="true">
                               @foreach($category->subCategoryAccounts as $subCategory)
                                    <div class="accordion-panel">
                                        <div class="accordion-heading" style="background-color: #e0e7eb;" role="tab" id="headingOne">
                                            <h3 class="card-title accordion-title">
                                                <a class="accordion-msg waves-effect waves-dark active" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    <h5 class="card-header-text">{{$subCategory->coa_name}}</h5>
                                                </a>
                                            </h3>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse active in collapse show" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="accordion-content accordion-desc">
                                                <div class="card-block">
                                                    <div class="view-info">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="general-info">
                                                                    <div class="row">
                                                                        <div class="col-lg-12 ">
                                                                            <div class="table-responsive">
                                                                                <table class="table m-0">
                                                                                    <tbody>
                                                                                        @foreach($subCategory->accounts as $account)
                                                                                            <tr>
                                                                                                <th scope="row">{{$account->coa_name}}</th>
                                                                                                <td>Cash you haven’t deposited in the bank. Add your bank and credit card accounts to accurately categorize transactions that aren't cash.</td>
                                                                                            </tr>
                                                                                        @endforeach
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <a style="float: left; padding: 8px;" class="btn btn-success modalLink" data-modal-size="modal-md" href="{{url('addAccountModal',['parentId'=>$subCategory->id])}}">
                                                                                Add account
                                                                            </a>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
        </div>
    </div>
	</div>
</div>
@endSection