@extends('backEnd.master')
@section('mainContent')
<div class="row">

	<div class="col-xl-3 col-md-6">
		<div class="card">
			<div class="card-block">
				<div class="row align-items-center m-l-0">
					<div class="col-auto">
						<i class="fa fa-book f-30 text-c-purple"></i>
					</div>
					<div class="col-auto">
						<h6 class="text-muted m-b-10">Total Income</h6>
						<h2 class="m-b-0">$4562379</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xl-3 col-md-6">
		<div class="card">
			<div class="card-block">
				<div class="row align-items-center m-l-0">
					<div class="col-auto">
						<i class="fa fa-rocket f-30 text-c-green"></i>
					</div>
					<div class="col-auto">
						<h6 class="text-muted m-b-10">Total Expense</h6>
						<h2 class="m-b-0">$205382</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xl-3 col-md-6">
		<div class="card">
			<div class="card-block">
				<div class="row align-items-center m-l-0">
					<div class="col-auto">
						<i class="fa fa-user f-30 text-c-red"></i>
					</div>
					<div class="col-auto">
						<h6 class="text-muted m-b-10">Total Employee</h6>
						<h2 class="m-b-0">594</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xl-3 col-md-6">
		<div class="card">
			<div class="card-block">
				<div class="row align-items-center m-l-0">
					<div class="col-auto">
						<i class="fa fa-lightbulb-o f-30 text-c-blue"></i>
					</div>
					<div class="col-auto">
						<h6 class="text-muted m-b-10">Total Project</h6>
						<h2 class="m-b-0">56</h2>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-xl-8 col-md-12">
		<div class="card">
			<div class="card-header">
				<h5>Analytics</h5>
				
				<div class="card-header-right">
					<ul class="list-unstyled card-option">
						<li><i class="fa fa fa-wrench open-card-option"></i></li>
						<li><i class="fa fa-window-maximize full-card"></i></li>
						<li><i class="fa fa-minus minimize-card"></i></li>
						<li><i class="fa fa-refresh reload-card"></i></li>
						<li><i class="fa fa-trash close-card"></i></li>
					</ul>
				</div>
			</div>
			<div class="card-block">
				<div id="sales-analytics" style="height: 400px;"></div>
			</div>
		</div>
	</div>
	<div class="col-xl-4 col-md-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col">
						<h4>$256.23</h4>
						<p class="text-muted">This Month</p>
					</div>
					<div class="col-auto">
						<label class="label label-success">+20%</label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<canvas id="this-month" style="height: 150px;"></canvas>
					</div>
				</div>
			</div>
		</div>
		<div class="card quater-card">
			<div class="card-block">
				<h6 class="text-muted m-b-15">Revenue</h6>
				<h4>$3,9452.50</h4>
				<p class="text-muted">$3,9452.50</p>
				<h5>87</h5>
				<p class="text-muted">Online Revenue<span class="f-right">80%</span></p>
				<div class="progress">
					<div class="progress-bar bg-c-blue" style="width: 80%"></div>
				</div>
				<h5 class="m-t-15">68</h5>
				<p class="text-muted">Offline Revenue<span class="f-right">50%</span></p>
				<div class="progress">
					<div class="progress-bar bg-c-green" style="width: 50%"></div>
				</div>
			</div>
		</div>
	</div>


	<div class="col-xl-8 col-md-12">
		<div class="card table-card">
			<div class="card-header">
				<h5>Projects</h5>
				<div class="card-header-right">
					<ul class="list-unstyled card-option">
						<li><i class="fa fa fa-wrench open-card-option"></i></li>
						<li><i class="fa fa-window-maximize full-card"></i></li>
						<li><i class="fa fa-minus minimize-card"></i></li>
						<li><i class="fa fa-refresh reload-card"></i></li>
						<li><i class="fa fa-trash close-card"></i></li>
					</ul>
				</div>
			</div>
			<div class="card-block">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>
									<div class="chk-option">
										<div class="checkbox-fade fade-in-primary">
											<label class="check-task">
												<input type="checkbox" value="">
												<span class="cr">
													<i class="cr-icon fa fa-check txt-default"></i>
												</span>
											</label>
										</div>
									</div>
								Assigned</th>
								<th>Name</th>
								<th>Due Date</th>
								<th class="text-right">Priority</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="chk-option">
										<div class="checkbox-fade fade-in-primary">
											<label class="check-task">
												<input type="checkbox" value="">
												<span class="cr">
													<i class="cr-icon fa fa-check txt-default"></i>
												</span>
											</label>
										</div>
									</div>
									<div class="d-inline-block align-middle">
										<img src="{{asset('public/assets/images/avatar-4.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
										<div class="d-inline-block">
											<h6>Omer Raju</h6>
											<p class="text-muted m-b-0">Graphics Designer</p>
										</div>
									</div>
								</td>
								<td>Able Pro</td>
								<td>Jun, 26</td>
								<td class="text-right"><label class="label label-danger">Low</label></td>
							</tr>
							<tr>
								<td>
									<div class="chk-option">
										<div class="checkbox-fade fade-in-primary">
											<label class="check-task">
												<input type="checkbox" value="">
												<span class="cr">
													<i class="cr-icon fa fa-check txt-default"></i>
												</span>
											</label>
										</div>
									</div>
									<div class="d-inline-block align-middle">
										<img src="{{asset('public/assets/images/avatar-5.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
										<div class="d-inline-block">
											<h6>Faisal Sarkar</h6>
											<p class="text-muted m-b-0">Web Designer</p>
										</div>
									</div>
								</td>
								<td>Mashable</td>
								<td>March, 31</td>
								<td class="text-right"><label class="label label-primary">high</label></td>
							</tr>
							<tr>
								<td>
									<div class="chk-option">
										<div class="checkbox-fade fade-in-primary">
											<label class="check-task">
												<input type="checkbox" value="">
												<span class="cr">
													<i class="cr-icon fa fa-check txt-default"></i>
												</span>
											</label>
										</div>
									</div>
									<div class="d-inline-block align-middle">
										<img src="{{asset('public/assets/images/avatar-3.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
										<div class="d-inline-block">
											<h6>Faisal Sarkar</h6>
											<p class="text-muted m-b-0">Developer</p>
										</div>
									</div>
								</td>
								<td>Flatable</td>
								<td>Aug, 02</td>
								<td class="text-right"><label class="label label-success">medium</label></td>
							</tr>
							<tr>
								<td>
									<div class="chk-option">
										<div class="checkbox-fade fade-in-primary">
											<label class="check-task">
												<input type="checkbox" value="">
												<span class="cr">
													<i class="cr-icon fa fa-check txt-default"></i>
												</span>
											</label>
										</div>
									</div>
									<div class="d-inline-block align-middle">
										<img src="{{asset('public/assets/images/avatar-2.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
										<div class="d-inline-block">
											<h6>hasan Masud</h6>
											<p class="text-muted m-b-0">Developer</p>
										</div>
									</div>
								</td>
								<td>Guruable</td>
								<td>Sep, 22</td>
								<td class="text-right"><label class="label label-primary">high</label></td>
							</tr>
						</tbody>
					</table>
					<div class="text-right m-r-20">
						<a href="#!" class=" b-b-primary text-primary">View all Projects</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-4 col-md-12">
		<div class="card ">
			<div class="card-header">
				<h5>Users</h5>
				<div class="card-header-right">
					<ul class="list-unstyled card-option">
						<li><i class="fa fa fa-wrench open-card-option"></i></li>
						<li><i class="fa fa-window-maximize full-card"></i></li>
						<li><i class="fa fa-minus minimize-card"></i></li>
						<li><i class="fa fa-refresh reload-card"></i></li>
						<li><i class="fa fa-trash close-card"></i></li>
					</ul>
				</div>
			</div>
			<div class="card-block">
				<div class="align-middle m-b-30">
					<img src="{{asset('public/assets/images/avatar-5.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
					<div class="d-inline-block">
						<h6>hasan Masud</h6>
						<p class="text-muted m-b-0">Developer</p>
					</div>
				</div>
				<div class="align-middle m-b-30">
					<img src="{{asset('public/assets/images/avatar-2.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
					<div class="d-inline-block">
						<h6>hasan Masud</h6>
						<p class="text-muted m-b-0">Developer</p>
					</div>
				</div>
				<div class="align-middle m-b-30">
					<img src="{{asset('public/assets/images/avatar-4.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
					<div class="d-inline-block">
						<h6>hasan Masud</h6>
						<p class="text-muted m-b-0">Developer</p>
					</div>
				</div>
				<div class="align-middle m-b-30">
					<img src="{{asset('public/assets/images/avatar-5.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
					<div class="d-inline-block">
						<h6>hasan Masud</h6>
						<p class="text-muted m-b-0">Developer</p>
					</div>
				</div>
				<div class="align-middle m-b-10">
					<img src="{{asset('public/assets/images/avatar-3.jpg')}}" alt="user image" class="img-radius img-40 align-top m-r-15">
					<div class="d-inline-block">
						<h6>hasan Masud</h6>
						<p class="text-muted m-b-0">Developer</p>
					</div>
				</div>
				<div class="text-center">
					<a href="#!" class="b-b-primary text-primary">View all Projects</a>
				</div>
			</div>
		</div>
	</div>


	<div class="col-xl-6 col-md-12">
		<div class="row">
			<div class="col-md-6">
				<div class="card text-center order-visitor-card">
					<div class="card-block">
						<h6 class="m-b-0">Total Subscription</h6>
						<h4 class="m-t-15 m-b-15"><i class="fa fa-arrow-down m-r-15 text-c-red"></i>7652</h4>
						<p class="m-b-0">48% From Last 24 Hours</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card text-center order-visitor-card">
					<div class="card-block">
						<h6 class="m-b-0">Order Status</h6>
						<h4 class="m-t-15 m-b-15"><i class="fa fa-arrow-up m-r-15 text-c-green"></i>6325</h4>
						<p class="m-b-0">36% From Last 6 Months</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card text-center order-visitor-card">
					<div class="card-block">
						<h6 class="m-b-0">Monthly Earnings</h6>
						<h4 class="m-t-15 m-b-15"><i class="fa fa-arrow-up m-r-15 text-c-green"></i>5963</h4>
						<p class="m-b-0">36% From Last 6 Months</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card text-center order-visitor-card">
					<div class="card-block">
						<h6 class="m-b-0">Unique Visitors</h6>
						<h4 class="m-t-15 m-b-15"><i class="fa fa-arrow-down m-r-15 text-c-red"></i>652</h4>
						<p class="m-b-0">36% From Last 6 Months</p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-block">
						<div class="row align-items-center m-l-0">
							<div class="col-auto">
								<i class="fa fa-user f-30 text-c-red"></i>
							</div>
							<div class="col-auto">
								<h6 class="text-muted m-b-10">Total Users</h6>
								<h2 class="m-b-0">10</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-block">
						<div class="row align-items-center m-l-0">
							<div class="col-auto">
								<i class="fa fa-lightbulb-o f-30 text-c-blue"></i>
							</div>
							<div class="col-auto">
								<h6 class="text-muted m-b-10">Total Clients</h6>
								<h2 class="m-b-0">325</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-xl-6 col-md-12">
		<div class="card">
			<div class="card-header">
				<h5>To Do List</h5>
				<div class="card-header-right">
					<ul class="list-unstyled card-option">
						<li><i class="fa fa fa-wrench open-card-option"></i></li>
						<li><i class="fa fa-window-maximize full-card"></i></li>
						<li><i class="fa fa-minus minimize-card"></i></li>
						<li><i class="fa fa-refresh reload-card"></i></li>
						<li><i class="fa fa-trash close-card"></i></li>
					</ul>
				</div>
			</div>
			<div class="card-block widget-last-task">
				<div class="to-do-list">
					<div class="checkbox-fade fade-in-default">
						<label class="check-task">
							<input type="checkbox" value="">
							<span class="cr">
								<i class="cr-icon fa fa-check txt-default"></i>
							</span>
							<span>Check your Email</span>
						</label>
					</div>
					<div class="f-right">
						<a href="#!" class="delete_todolist"><i class="fa fa-trash"></i></a>
					</div>
				</div>
				<div class="to-do-list">
					<div class="checkbox-fade fade-in-default">
						<label class="check-task">
							<input type="checkbox" value="">
							<span class="cr">
								<i class="cr-icon fa fa-check txt-default"></i>
							</span>
							<span>Make YouTube Video</span>
						</label>
					</div>
					<div class="f-right">
						<a href="#!" class="delete_todolist"><i class="fa fa-trash"></i></a>
					</div>
				</div>
				<div class="to-do-list">
					<div class="checkbox-fade fade-in-default">
						<label class="check-task">
							<input type="checkbox" value="">
							<span class="cr">
								<i class="cr-icon fa fa-check txt-default"></i>
							</span>
							<span>Create Banner</span>
						</label>
					</div>
					<div class="f-right">
						<a href="#!" class="delete_todolist"><i class="fa fa-trash"></i></a>
					</div>
				</div>
				<div class="to-do-list">
					<div class="checkbox-fade fade-in-default">
						<label class="check-task">
							<input type="checkbox" value="">
							<span class="cr">
								<i class="cr-icon fa fa-check txt-default"></i>
							</span>
							<span>Upload Project</span>
						</label>
					</div>
					<div class="f-right">
						<a href="#!" class="delete_todolist"><i class="fa fa-trash"></i></a>
					</div>
				</div>
				<div class="right-icon-control">
					<form class="form-material">
						<div class="form-group form-primary">
							<input type="text" name="footer-email" class="form-control" required="">
							<span class="form-bar"></span>
							<label class="float-label">Add Task</label>
						</div>
					</form>
					<div class="form-icon ">
						<button class="btn btn-primary btn-icon  waves-effect waves-light">
							<i class="fa fa-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection