$(document).ready(function() {

   $("#email").blur(function(e) {

       let url = $('#url').val(),
           email = e.target.value,
           emailType = getEmailType(e);

       $.ajax({

           url: url+'/checkDuplicateEmail',
           type: 'get',
           data: {
               email: email,
               emailType: emailType
           },
           success: function(response={}) {
               if(response && response.duplicateEmail > 0) {

                   $("#email").after("<p id='duplicate-warning' style='color: red'>The email already exists</p>");

               } else if (response && response.duplicateEmail <= 0) {

                   $("#duplicate-warning").remove();

               }
           },
           error: function(error) {
               console.log("Something went wrong calling duplicate: ", error);
           }

       });

   });

   function getEmailType(e) {

       if(e.target.classList.contains('employee_email')) return 'employee';
       else return 'user';

   }

});