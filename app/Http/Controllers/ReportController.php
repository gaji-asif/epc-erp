<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\ErpTransaction;
use App\ErpTransactionDetails as TransactionDetails;

class ReportController extends Controller
{
    public function generateTrialBalance() {

        $transactions = TransactionDetails::whereMonth('created_at',Carbon::now()->month)->get();

        return view('backEnd.reports.trialBalance',[

            'transactions' => $transactions

        ]);


    }
}
