<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpLeaveTypes;
use Auth;


class ErpLeaveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leave_types = ErpLeaveTypes::where('active_status',1)->get();
        return view('backEnd.employees.leave_type.index', compact('leave_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type'=>'required',
            'total_days'=>'required'
        ]);
            
        $leave_type = new ErpLeaveTypes();
        $leave_type->type = $request->get('type');
        $leave_type->total_days = $request->get('total_days');
        $leave_type->created_by = Auth::user()->id;

        $result = $leave_type->save();
        if($result) {
            return redirect('/leave_type')->with('message-success', 'Type has been added.');
        } else {
            return redirect('/leave_type')->with('message-success', 'Something went wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = ErpLeaveTypes::find($id);
        $leave_types = ErpLeaveTypes::where('active_status',1)->get();
        return view('backEnd.employees.leave_type.index', compact('editData','leave_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type'=>'required',
            'total_days'=>'required'
        ]);
            
        $leave_type = ErpLeaveTypes::find($id);
        $leave_type->type = $request->get('type');
        $leave_type->total_days = $request->get('total_days');
        $leave_type->updated_by = Auth::user()->id;

        $result = $leave_type->update();
        if($result) {
            return redirect('/leave_type')->with('message-success', 'Type has been updated.');
        } else {
            return redirect('/leave_type')->with('message-success', 'Something went wrong.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteLeaveTypeView($id){
        $module = 'deleteLeaveType';
         return view('backEnd.showDeleteModal', compact('id','module'));
    }

    public function deleteLeaveType($id){
        $leave_type = ErpLeaveTypes::find($id);
        $leave_type->active_status = 0;

        $result = $leave_type->update();
        if($result){
            return redirect()->back()->with('message-success-delete', 'Type has been deleted successfully');
        }else{
            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }
    }
}
