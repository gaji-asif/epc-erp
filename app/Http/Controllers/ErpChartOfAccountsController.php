<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpAccountsCategory;
use App\ErpAccountsClass;
use App\ErpChartOfAccounts;
use App\ErpProject;
use Auth;

class ErpChartOfAccountsController extends Controller
{
    public function accountList(){
        $coas = ErpChartOfAccounts::all();
        return view('backEnd.chart_of_accounts.accountList', compact('coas'));
    }

    public function addNewCoa(){
        $category = ErpAccountsCategory::all();
        $coaClass = ErpAccountsClass::all();
        $allChartsOfAccounts = ErpChartOfAccounts::all();
        $projects = ErpProject::where('active_status', '=', 1)->get();
        return view('backEnd.chart_of_accounts.addNewCoa', compact('category', 'coaClass', 'allChartsOfAccounts', 'projects'));
    }
    public function saveCoaData(Request $request){
        $request->validate([
            'coa_name' => "required",
            //'coa_category' => "required",
            'project_id' => "required"
        ]);

        $chartOfAccounts = new ErpChartOfAccounts();
        $chartOfAccounts->coa_name = $request->coa_name;
        $chartOfAccounts->coa_category = $request->coa_category ? $request->coa_category : 0;
        $chartOfAccounts->project_id = $request->project_id;
        $chartOfAccounts->coa_class = $request->coa_class;
        $chartOfAccounts->coa_control = $request->coa_control;
        $chartOfAccounts->coa_parent = $request->coa_parent;
        /*$chartOfAccounts->coa_level = $request->coa_level;*/
        $chartOfAccounts->created_by = Auth::user()->id;
        $chartOfAccounts->account_class = $request->account_class;
        $debit_credit_checked = $request->debit_credit_amount;

        if($debit_credit_checked == 'debit') {

            $chartOfAccounts->shadow_debit_amount = $request->shadow_debit_amount;
            $chartOfAccounts->debit_amount = 1;

            if(empty($request->open_debit_amount)){
                $chartOfAccounts->opening_debit_amount = 0;
            }
            else{
                $chartOfAccounts->opening_debit_amount = $request->open_debit_amount;
            }

        } else {
         
            $chartOfAccounts->shadow_credit_amount = $request->shadow_credit_amount;
            $chartOfAccounts->credit_amount = 1;
            if($request->open_credit_amount == '' || $request->open_credit_amount == NULL){
                $chartOfAccounts->opening_credit_amount = 0;
            }
            else{
                $chartOfAccounts->opening_credit_amount = $request->open_credit_amount;
            }
        }

        $results = $chartOfAccounts->save();
        if($results){
            return back()->with('message-success', 'New Chart of Accounts has been added successfully');
        }else{
            return back()->with('message-danger', 'Something went wrong, please try again');
        }
   }

   public function coa_view() {

        return view('backEnd.chart_of_accounts.coa_view',['categories' => ErpAccountsCategory::all()]);
   }

   public function showAddAccountModal($parentId = null) {

            return view('backEnd.addAccountModal', [
                'parentId' => $parentId,
                'categories' => ErpAccountsCategory::all(),
                'projects' => ErpProject::all()

            ]);

   }
}