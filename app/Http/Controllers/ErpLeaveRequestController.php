<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpLeaveRequest;
use App\ErpLeaveTypes;
use Auth;
use App\User;
use Session;

class ErpLeaveRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $get_user_role_id = User::select('role_id')->where('id',$user_id)->get();
        $user_role_id = $get_user_role_id[0]['role_id'];
        if($user_role_id == 1) {
            $leave_requests = ErpLeaveRequest::where('active_status',1)->get();
            $users = User::where('active_status', 1)->get();
        } else {
            $leave_requests = ErpLeaveRequest::where('employee_id', $user_id)->where('active_status',1)->get();
        }
        $leave_types = ErpLeaveTypes::where('active_status',1)->get();
        return view('backEnd.employees.leave_request.index', compact('leave_requests','leave_types','user_role_id','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leave_types = ErpLeaveTypes::where('active_status',1)->get();
        return view('backEnd.employees.leave_request.create',compact('leave_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type_id'=>'required',
            'apply_date'=> 'required',
            'leave_from'=> 'required',
            'leave_to'=> 'required',
            'reason'=> 'required'
        ]);

        $leave_request = new ErpLeaveRequest();
        $leave_request->employee_id = Auth::user()->id;
        $leave_request->type_id = $request->get('type_id');
        $leave_request->apply_date = self::check_date_value( $request->get('apply_date') );
        $leave_request->leave_from = self::check_date_value( $request->get('leave_from') );
        $leave_request->leave_to = self::check_date_value( $request->get('leave_to') );
        $leave_request->reason = $request->get('reason');
        $leave_request->approve_status = 'P';
        $invalid_extentension_flag = 0;
        if ($request->hasFile('file')) {
            $extensions = ["pdf", "jpg","jpeg","png","gif","tiff", "svg", "doc", "docx"];
            $file = $request->file('file');
            $file_name = $file->getClientOriginalName();
            $file_extension_name = $file->getClientOriginalExtension();
            $file_size = $request->file('file')->getSize();
            $file_size_in_kb = round( ( $file_size / 1024 ), 2);

            if(! in_array($file_extension_name, $extensions) || $file_size_in_kb > 9000){
              $invalid_extentension_flag = 1;
            }

            $destinationPath = public_path('/uploads/files');
            $imagePath = $destinationPath. "/".  $file_name;
            if ($invalid_extentension_flag == 0) {
                $file->move($destinationPath, $file_name);
                $leave_request->file = '/public/uploads/files/'.$file_name;
            }
        }

        $leave_request->created_by = Auth::user()->id;

        $results = $leave_request->save();
        if($results) {
            if ($invalid_extentension_flag == 1) return redirect('leave_request')->with('message-success', 'Request has been added but uploaded file is not valid.');
            else return redirect('leave_request')->with('message-success', 'Request has been added');
        } else {
            return redirect('leave_request')->with('message-danger', 'Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = ErpLeaveRequest::find($id);
        $leave_types = ErpLeaveTypes::where('active_status',1)->get();
        return view('backEnd.employees.leave_request.edit',compact('editData', 'leave_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type_id'=>'required',
            'apply_date'=> 'required',
            'leave_from'=> 'required',
            'leave_to'=> 'required',
            'reason'=> 'required'
        ]);

        $leave_request = ErpLeaveRequest::find($id);
        $leave_request->employee_id = Auth::user()->id;
        $leave_request->type_id = $request->get('type_id');
        $leave_request->apply_date = self::check_date_value( $request->get('apply_date') );
        $leave_request->leave_from = self::check_date_value( $request->get('leave_from') );
        $leave_request->leave_to = self::check_date_value( $request->get('leave_to') );
        $leave_request->reason = $request->get('reason');
        $leave_request->approve_status = 'P';
        
        $invalid_extentension_flag = 0;
        if ($request->hasFile('file')) {
            $extensions = ["pdf", "jpg","jpeg","png","gif","tiff", "svg", "doc", "docx"];
            $file = $request->file('file');
            $file_name = $file->getClientOriginalName();
            $file_extension_name = $file->getClientOriginalExtension();
            $file_size = $request->file('file')->getSize();
            $file_size_in_kb = round( ( $file_size / 1024 ), 2);

            if(! in_array($file_extension_name, $extensions) || $file_size_in_kb > 9000 ){
              $invalid_extentension_flag = 1;
            }

            $destinationPath = public_path('/uploads/files');
            $imagePath = $destinationPath. "/".  $file_name;
            if ($invalid_extentension_flag == 0) {
                $file->move($destinationPath, $file_name);
                $leave_request->file = '/public/uploads/files/'.$file_name;
            }
        }

        $leave_request->updated_by = Auth::user()->id;

        $results = $leave_request->update();
        if($results) {
            if ($invalid_extentension_flag == 1) return redirect('leave_request')->with('message-success', 'Request has been updated but uploaded file is not valid.');
            else return redirect('leave_request')->with('message-success', 'Request has been added');
        } else {
            return redirect('leave_request')->with('message-danger', 'Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteLeaveRequestView($id){
        $module = 'deleteLeaveRequest';
        return view('backEnd.showDeleteModal', compact('id','module'));
    }

    public function deleteLeaveRequest($id){
        $leave_request = ErpLeaveRequest::find($id);
        $leave_request->active_status = 0;

        $results = $leave_request->update();
        if($results){
            return redirect()->back()->with('message-success-delete', 'Request has been deleted successfully');
        }else{
            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }
    }

    // Customized function for checking date null or not
    public function check_date_value( $date_value ) {
        if (isset($date_value)) {
            return date('Y-m-d', strtotime($date_value));
        } else {
            return $date_value;
        }
    }
}
