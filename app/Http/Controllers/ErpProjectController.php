<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpProject;
use App\ErpClient;
use Auth;

class ErpProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = ErpProject::all();
        return view('backEnd.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = ErpClient::where('active_status', '=', '1')->get();
        return view('backEnd.projects.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'project_name'=>'required',
            'project_start_date'=> 'required',
            'project_end_date'=> 'required',
            // 'project_status' => 'required|integer',
            'project_amount' => 'required',
            'client_id' => 'required'
            // 'company_id' => 'required'
        ]);

        $project = new ErpProject();
        $project->project_name = $request->get('project_name');
        $project->project_start_date = date('Y-m-d', strtotime($request->project_start_date));
        // $project->project_status = $request->get('project_status');
        $project->project_end_date = date('Y-m-d', strtotime($request->project_end_date));
        $project->project_amount = $request->get('project_amount');
        $project->client_id = $request->get('client_id');
        $project->advances_received = $request->get('advances_received');
        $project->last_date_of_receipt = date('Y-m-d', strtotime($request->last_date_of_receipt));
        $project->completion_due_date = date('Y-m-d', strtotime($request->completion_due_date));
        $project->completed_on = date('Y-m-d', strtotime($request->completed_on));
        $project->receipts_to_date = $request->get('receipts_to_date');
        $project->expenses_to_date = $request->get('expenses_to_date');
        $project->created_by = Auth::user()->id;

        $project->save();
        return redirect('/project')->with('message-success', 'Project has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $editData = ErpProject::find($id);
        $clients = ErpClient::where('active_status', '=', '1')->get();
        return view('backEnd.projects.show', compact('editData', 'clients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = ErpProject::find($id);
        $clients = ErpClient::where('active_status', '=', '1')->get();
        return view('backEnd.projects.edit', compact('editData', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'project_name'=>'required',
            'project_start_date'=> 'required|date',
            'project_end_date'=> 'required',
            // 'project_status' => 'required|integer',
            'project_amount' => 'required|integer',
            'client_id' => 'required'
            // 'company_id' => 'required'
        ]);

        $project = ErpProject::find($id);
        $project->project_name = $request->get('project_name');
        $project->project_start_date = date('Y-m-d', strtotime($request->project_start_date));
        // $project->project_status = $request->get('project_status');
        $project->project_end_date = date('Y-m-d', strtotime($request->project_end_date));
        $project->project_amount = $request->get('project_amount');
        $project->client_id = $request->get('client_id');
        $project->advances_received = $request->get('advances_received');
        $project->last_date_of_receipt = date('Y-m-d', strtotime($request->last_date_of_receipt));
        $project->completion_due_date = date('Y-m-d', strtotime($request->completion_due_date));
        $project->completed_on = date('Y-m-d', strtotime($request->completed_on));
        $project->receipts_to_date = $request->get('receipts_to_date');
        $project->expenses_to_date = $request->get('expenses_to_date');
        $project->updated_by = Auth::user()->id;

        $project->save();
        return redirect('/project')->with('message-success', 'Project has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $project = ErpProject::find($id);
    //     $project->delete();

    //     return redirect('/project')->with('message-success', 'Project has been deleted Successfully');
    // }

    public function deleteProjectView($id){
         return view('backEnd.projects.deleteProjectView', compact('id'));
    }

    public function deleteProject($id){
        $result = ErpProject::destroy($id);
        if($result){
            return redirect()->back()->with('message-success-delete', 'Project has been deleted successfully');
        }else{
            return redirect()->back()->with('message-danger-delete', 'Something went wrong, please try again');
        }
    }

}
