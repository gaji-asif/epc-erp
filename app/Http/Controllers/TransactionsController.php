<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ErpTransaction as Transaction;
use App\ErpProject as Project;
use App\ErpAccountsCategory as Category;
use App\Services\AddTransactionsService as TransactionService;

class TransactionsController extends Controller
{
    protected $transactionService;

    public function __construct(TransactionService $transactionsService)
    {

        $this->transactionService = $transactionsService;

    }

    public function index(Request $request) {
        $transactionForm = $request->transactionType ? $request->transactionType : 'journal';
        $voucherNo = 1;
        $lastTransaction = Transaction::latest()->first();
        if($request->transactionType) {

            $transactionForm = $request->transactionType;

        }

        if($lastTransaction) {

            $voucherNo = ++$lastTransaction->voucher_no;

        }

        return view('backEnd.transaction.main',[

            'projects' => Project::all(),
            'categories' => Category::all(),
            'transactionForm' => $transactionForm,
            'voucherNo' => $voucherNo

        ]);

    }


    public function addTransactions(Request $request) {

        $request->validate([

            'project' => 'required',
            'transaction_date' => 'required',
            'voucher_no' => 'required',
            'transactionType' => 'required'
            //'total_debit' => 'required|same:total_credit'

        ]);

        $isTransactionAdded = $this->transactionService->createTransactions($request);

        if($isTransactionAdded) return redirect()->route('addTransactionView',['transactionType'=>$request->transactionType]);

        else return "Transaction could not be added";


    }
}
