<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErpAccountsCategory extends Model
{
    public function subCategoryAccounts() {

        return $this->hasMany('App\ErpChartOfAccounts','coa_category','id');

    }
}
