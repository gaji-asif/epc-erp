<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

class AddTransactionsService {

    public function createTransactions($request) {
        $errors = new MessageBag();

        DB::beginTransaction();

        try {

            $totalTransactions = $request->totalRow;

            $user = Auth::user();

            $erpTransaction = $user->createTransaction($request);

            $record = 0;
            $i = 0;

            while (true) {

                $dataForDetail = [];

                if($request["debit$i"] || $request["credit$i"]) {

                    $dataForDetail['coa_parent'] = $request["coa_parent$i"];

                    $dataForDetail['debit_amount'] = $request["debit$i"] ? $request["debit$i"] : 0;

                    $dataForDetail['credit_amount'] = $request["credit$i"] ? $request["credit$i"]: 0;

                    if($dataForDetail['credit_amount'] > 0) {

                        $dataForDetail['type'] = 'C';

                    } else {

                        $dataForDetail['type'] = 'D';

                    }

                    $erpTransaction->addTransactionDetail($dataForDetail);

                    if($record == $totalTransactions) break;

                    $record++;
                } else {

                    $errors->add('transactionError', '0 is not allowed value');

                    return back()->withErrors($errors);

                }
                $i++;

            }
        } catch (\Exception $e) {

            DB::rollBack();

            $errors->add('transactionError', 'Transaction could not take place');

            return back()->withErrors($errors);

        }

        DB::commit();

        return $erpTransaction;

    }

}