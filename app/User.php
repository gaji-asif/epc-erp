<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function transaction() {

        return $this->hasMany('App\ErpTransaction','created_by','id');

    }

    public function createTransaction($request) {

        return $this->transaction()->create([

            'transaction_date' => Carbon::createFromFormat('d-m-Y',$request->transaction_date)->format('Y-m-d'),

            'voucher_no' => $request->voucher_no,

            'total_transaction' => $request->total_debit > 0 ? $request->total_debit : $request->total_credit,

            'project' => $request->project

        ]);

    }
}
