<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErpTransaction extends Model
{
    protected $fillable = [
        'transaction_date',
        'description',
        'voucher_no',
        'total_transaction',
        'project_id',
        'active_status',
        'created_by'];


    public function transactionDetail() {

        return $this->hasMany('App\ErpTransactionDetails','transaction_id','id');

    }


    public function addTransactionDetail($detailData=[]) {

        $this->transactionDetail()->create([

            'coa_id' => $detailData['coa_parent'],

            'debit_amount' => $detailData['debit_amount'],

            'credit_amount' => $detailData['credit_amount'],

            'type' => $detailData['type']

        ]);

    }


}
